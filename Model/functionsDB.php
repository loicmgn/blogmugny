<?php

/*
 *  M152 Blog
  Mugny Loïc
  January - March 2019

/**
 * Crée et conserve une instance de connection à la base de données
 * @return PDO la connection
 */
include 'mySql.inc';
include '../Classes/Post.php';


function myDatabase() {
    static $dbc = null;

    if ($dbc == null) {
        try {
            $dbc = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_PERSISTENT => true));
            $dbc ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo 'Erreur : ' . $e->getMessage() . '<br />';
            echo 'N° : ' . $e->getCode();
            die('Could not connect to MySQL');
        }
    }
    return $dbc;
}

/**
 * Enregistre un utilisateur en base
 * @return boolean si tout est ok ou pas
 */
function insertPost($comment) {
    //Créer le string contenant l'insertion en base de l'utilisateur (cf Ressources)
    //Si l'exécution de la requête fonctionne
    //Début Si
    //Retourne vrai
    //Fin Si
    //Début Sinon
    //Retourne faux
    //Fin Sinon
    $connect = myDatabase();
    try {
        $connect->beginTransaction();
        $req = $connect->prepare('INSERT INTO `post`(`commentaire`) VALUES (:comment)');
        $req->bindParam(':comment', $comment, PDO::PARAM_STR);
        $req->execute();
        $return = $connect->lastInsertId();
        $connect->commit();
        RETURN $return;
    } catch (Exception $ex) {
        echo $ex ->getMessage();
        $connect->rollBack();
        return FALSE;
    }
}
function insertMedia($nameMedia, $typeMedia, $idPost) {
    //Créer le string contenant l'insertion en base de l'utilisateur (cf Ressources)
    //Si l'exécution de la requête fonctionne
    //Début Si
    //Retourne vrai
    //Fin Si
    //Début Sinon
    //Retourne faux
    //Fin Sinon
    try {
        myDatabase()->beginTransaction();
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `media`(`nomFichierMedia`, `typeMedia`, `idPost`) VALUES (:nameMedia, :typeMedia, :idPost)');
        $req->bindParam(':nameMedia', $nameMedia, PDO::PARAM_STR);
        $req->bindParam(':typeMedia', $typeMedia, PDO::PARAM_STR);
        $req->bindParam(':idPost', $idPost, PDO::PARAM_STR);
        $req->execute();
        myDatabase()->commit();
        RETURN TRUE;
    } catch (Exception $ex) {
        echo $ex ->getMessage();
        myDatabase()->rollBack();
        return FALSE;
    }
}

function getPosts(){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT post.idPost, commentaire, datePoste, nomFichierMedia FROM post LEFT OUTER JOIN media ON post.idPost = media.idPost");
        $req->execute();
        $result = $req->fetchAll();
        $posts = [];
        foreach ($result as $post) 
        {
            $singlePost = new Post();
            $singlePost->id = $post['idPost'];
            $singlePost->comment = $post['commentaire'];
            $singlePost->nomMedia = $post['nomFichierMedia'];
            $singlePost->datePosted = $post['datePoste'];
            array_push($posts, $singlePost);
        }
    } catch (Exception $ex) {
        return FALSE;
    }
    return $posts;
}

/**
 * Vérifie si l'utilisateur existe
 * @return boolean s'il existe ou pas
 */
/**function userExists($login, $pwd) {
    //Créer le string contenant la requête sélectionnant l'utilisateur possédant l'id passé en paramètre (cf Ressources)
    //Exécution de la requête qui retourne un "PDOStatement" et exécution de la méthode "fetchAll" sur ce PDOStatement
    //et finalement stockage du résultat dans une variable (cf Ressources)
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM USERS WHERE login = :login AND password = :password");
        $req->bindParam(':login', $login, PDO::PARAM_STR);
        $req->bindParam(':password', $pwd, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    //Si le tableau contient quelque chose (cf Ressources "count")
    //Début Si
    //Retourne vrai
    //Fin Si
    //Début Sinon
    //Retourne faux
    //Fin Sinon
    if ($result != null) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function getUserDetail($login){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM USERS WHERE login = :login");
        $req->bindParam(':login', $login, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    return $result;
}

function insertPost($titre, $description, $idUser){
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `news`(`title`, `description`, `idUser`) VALUES (:titre, :description, :idUser)');
        $req->bindParam(':titre', $titre, PDO::PARAM_STR);
        $req->bindParam(':description', $description, PDO::PARAM_STR);
        $req->bindParam(':idUser', $idUser, PDO::PARAM_INT);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}

function getPosts(){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM NEWS ORDER BY lastEditDate DESC");
        $req->execute();
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    return $result;
}

function getUserDetailsById($id){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM USERS WHERE idUser = :id");
        $req->bindParam(':id', $id, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    return $result;
}

function getPostById($idNews){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM NEWS WHERE idNews = :idNews");
        $req->bindParam(':idNews', $idNews, PDO::PARAM_INT);
        $req->execute();
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    return $result;
}
//UPDATE `news` SET `idNews`=[value-1],`title`=[value-2],`description`=[value-3],`lastEditDate`=[value-6] WHERE 1
function updatePost($idNews, $titre, $description, $date){
    try {
        $connect = myDatabase();
        $req = $connect->prepare('UPDATE `news` SET title= :titre,description=:description,lastEditDate=:currentDate WHERE idNews = :idNews');
        $req->bindParam(':idNews', $idNews, PDO::PARAM_INT);
        $req->bindParam(':titre', $titre, PDO::PARAM_STR);
        $req->bindParam(':description', $description, PDO::PARAM_STR);
        $req->bindParam(':currentDate', $date);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}

function deletePost($idNews){
    try {
        $connect = myDatabase();
        $req = $connect->prepare('DELETE FROM `news` WHERE idNews = :idNews');
        $req->bindParam(':idNews', $idNews, PDO::PARAM_INT);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}

?>
 **/